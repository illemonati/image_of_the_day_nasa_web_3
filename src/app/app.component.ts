import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'image-of-the-day-nasa-web3';

  ngOnInit(): void {
    this.startAnimate();
  }

  startAnimate() {
    setInterval(() =>
      // Set the color and increment the index.
      document.body.style.backgroundColor = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')', 1000);
  }
}
