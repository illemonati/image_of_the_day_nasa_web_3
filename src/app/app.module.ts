import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {IMOTDInterfaceComponent} from './imotd_interface/imotd_interface.component';

@NgModule({
  declarations: [
    AppComponent,
    IMOTDInterfaceComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
