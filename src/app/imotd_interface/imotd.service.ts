import { IMOTD } from './imotd';
import { Injectable } from '@angular/core';
import axios from 'axios';



@Injectable({
  providedIn: 'root',
})
export class IMOTDService {
  private static processUrl(url: string) {
    if (url.substring(0, 7) === 'http://') {
      url = 'https://' + url.substring(7);
    }
    return url;
  }

  public async getIMOTD(): Promise<IMOTD> {
    const url = 'https://cors-anywhere.herokuapp.com/www.nasa.gov/rss/dyn/lg_image_of_the_day.rss';
    const res = await axios.get(url);
    // axios.get(url).then((response) => res = response);
    let xmlDoc;
    const parser: DOMParser = new DOMParser();
    xmlDoc = parser.parseFromString(res.data, 'text/xml');
    const title: string = xmlDoc.getElementsByTagName('title')[1].firstChild.nodeValue;
    const description: string = xmlDoc.getElementsByTagName('description')[1].firstChild.nodeValue;
    const link: string = IMOTDService.processUrl(xmlDoc.getElementsByTagName('link')[1].firstChild.nodeValue);
    const pubDate: string = xmlDoc.getElementsByTagName('pubDate')[1].firstChild.nodeValue;
    const imageUrl: string = IMOTDService.processUrl(xmlDoc.getElementsByTagName('enclosure')[0].getAttribute('url'));
    console.log(imageUrl);
    const imotd = new IMOTD(
      imageUrl, title, description, link, pubDate
    );
    return imotd;
  }
}
