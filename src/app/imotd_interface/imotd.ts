
export class IMOTD {
  imageUrl: string;
  title: string;
  description: string;
  link: string;
  pubDate: string;
  imageBase64: any;

  constructor(imageUrl: string, title: string, description: string, link: string, pubDate: string) {
    this.imageUrl = imageUrl;
    this.title = title;
    this.description = description;
    this.link = link;
    this.pubDate = pubDate;
  }
}
