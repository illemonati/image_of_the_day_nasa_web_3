import {Component, OnInit} from '@angular/core';
import {IMOTD} from './imotd';
import {IMOTDService} from './imotd.service';

@Component({
  selector: 'app-imotd-interface',
  templateUrl: 'imotd_interface.html',
})
export class IMOTDInterfaceComponent implements OnInit {
  imotd: IMOTD = new IMOTD('image_url', 'title', 'description', 'link', 'pubDate');
  constructor(private imotdService: IMOTDService) {}
  rssReady = false;
  ngOnInit(): void {
    this.update();
    (window as any).update = () => this.update();
  }

  update(): void {
    this.imotdService.getIMOTD().then((imotd: IMOTD) => {this.imotd = imotd; this.rssReady = true; });
  }
}
